-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Jul 2020 pada 03.20
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pegawai`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_employee`
--

CREATE TABLE `data_employee` (
  `id` int(11) NOT NULL,
  `nik` varchar(19) NOT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `kelurahan` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `rt` int(11) NOT NULL,
  `rw` int(11) NOT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_employee`
--

INSERT INTO `data_employee` (`id`, `nik`, `agama`, `alamat`, `jenis_kelamin`, `kecamatan`, `kelurahan`, `name`, `pekerjaan`, `rt`, `rw`, `tanggal_lahir`, `tempat_lahir`) VALUES
(27, '4', 'budha', 'jl.maju', 'laki-laki', 'serpong', 'jelupang', 'hmm', 'web designer', 3, 3, '17 agustus 2003', 'jakarta selatan'),
(26, '3', 'hindu', 'jl.selamet', 'laki-laki', 'jelupang', 'pakujaya', 'budi', 'programmer', 4, 4, '05 april', 'jakarta'),
(30, '12', 'budha', 'jepang', 'perempuan', 'hmm', 'hmm', 'sagiri', 'ilustrator', 6, 6, '1 maret 2000', 'jepang'),
(25, '2', 'islam', 'jl.pahlawan', 'laki-laki', 'serpong', 'suka maju', 'rizki', 'siswa', 2, 13, '5 juli 2002', 'jakarta'),
(29, '12', 'zxv', 'sadf', 'sdf', 'sdf', 'afs', 'iki', 'sdz', 3, 3, 'asdf', 'adfs');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(31),
(1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--
-- Kesalahan membaca struktur untuk tabel pegawai.jabatan: #1932 - Table 'pegawai.jabatan' doesn't exist in engine
-- Kesalahan membaca data untuk tabel pegawai.jabatan: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `pegawai`.`jabatan`' at line 1

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_employee`
--
ALTER TABLE `data_employee`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_employee`
--
ALTER TABLE `data_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
