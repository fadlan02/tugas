package com.example.demo.service;

import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository repository;

    public Employee saveEmployee(Employee employee) {
        return repository.save(employee);
    }

    public List<Employee> saveEmployees(List<Employee> employees) {
        return repository.saveAll(employees);
    }

    public List<Employee> getEmployees() {
        return repository.findAll();
    }

    public Employee getEmployeeById(int id) {
        return repository.findById(id).orElse(null);
    }

    public Employee getEmployeeByName(String name) {
        return repository.findByName(name);
    }

    public String deleteEmployee(int id) {
        repository.deleteById(id);
        return "Data Employee Berhasil Dihapus";
    }

//    public Employee updateEmployee(Employee employee){
//        Employee existingEmployee = repository.findById(employee.getId()).orElse(null);
//        existingEmployee.setNik(employee.getNik());
//        existingEmployee.setName(employee.getName());
//        existingEmployee.setTempat_lahir(employee.getTempat_lahir());
//        existingEmployee.setTanggal_lahir(employee.getTanggal_lahir());
//        existingEmployee.setJenis_kelamin(employee.getJenis_kelamin());
//        existingEmployee.setAlamat(employee.getAlamat());
//        existingEmployee.setRt(employee.getRt());
//        existingEmployee.setRw(employee.getRw());
//        existingEmployee.setKelurahan(employee.getKelurahan());
//        existingEmployee.setKecamatan(employee.getKecamatan());
//        existingEmployee.setAgama(employee.getAgama());
//        existingEmployee.setPekerjaan(employee.getPekerjaan());
//
//        return repository.save(existingEmployee);
//    }
}
