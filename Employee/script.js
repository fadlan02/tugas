const getData = function () {
  fetch('http://localhost:8282/employees')
    .then(resp => resp.json())
    .then(data => showData(data))
}
getData();

const showData = (datas) => {
  datas.forEach(data => {
    console.log(data);
    document.getElementById("dataEmployee").innerHTML += `
    
    <div class="col-sm-3">
        <div class="card text-white bg-light mb-3" style="max-width: 18rem;">
            <div class="card-header" style="color: black;">NIK : ${data.nik}</div>
            <div class="card-body" style="color: black;">
                <h5 class="card-title">Nama : ${data.name}</h5>
                <p class="card-text">Alamat : ${data.alamat}</p>
                <button class="btn btn-info" data-toggle="modal"
                    data-target="#${data.name}Detail"><i class="fas fa-list"></i>
                </button>
                <button class="btn btn-warning" data-toggle="modal" data-target="#editData"> <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-danger" onclick="deleteData(${data.id})"><i
                        class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    
  <br>
  <div class="modal fade" id="${data.name}Detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data karyawan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p class="card-text">NIK : ${data.nik} </p>
      <p class="card-text">Nama : ${data.name}</p>
      <p class="card-text">Tempat lahir : ${data.tempat_lahir}</p>
      <p class="card-text">Tanggal lahir : ${data.tanggal_lahir}</p>
      <p class="card-text">Jenis kelamin : ${data.jenis_kelamin}</p>
      <p class="card-text">agama : ${data.agama}</p>
      <p class="card-text">alamat : ${data.alamat}</p>
      <p class="card-text">Kecamatan : ${data.kecamatan}</p>
      <p class="card-text">Kelurahan : ${data.kelurahan}</p>
      <p class="card-text">Pekerjaan : ${data.pekerjaan}</p>
      <p class="card-text">RT : ${data.rt}</p>
      <p class="card-text">RW : ${data.rw}</p>
       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>`
  });
}

function inputData() {
  let nik = document.getElementById("nik").value
  let name = document.getElementById("name").value
  let agama = document.getElementById("agama").value
  let alamat = document.getElementById("alamat").value
  let jenis_kelamin = document.getElementById("jenis_kelamin").value
  let kecamatan = document.getElementById("kecamatan").value
  let kelurahan = document.getElementById("kelurahan").value
  let pekerjaan = document.getElementById("pekerjaan").value
  let rt = document.getElementById("rt").value
  let rw = document.getElementById("rw").value
  let tanggal_lahir = document.getElementById("tanggal_lahir").value
  let tempat_lahir = document.getElementById("tempat_lahir").value


  if (name == "" || nik == "" || agama == "" || alamat == "" || jenis_kelamin == "" || kecamatan == "" || kelurahan == "" || pekerjaan == "" || rt == "" || rw == "" || tanggal_lahir == "" || tempat_lahir == "") {
    alert("Data tidak boleh kosong")
  }

  let str_json = JSON.stringify({
    nik: nik,
    name: name,
    tempat_lahir: tempat_lahir,
    tanggal_lahir: tanggal_lahir,
    jenis_kelamin: jenis_kelamin,
    alamat: alamat,
    rt: rt,
    rw: rw,
    kelurahan: kelurahan,
    kecamatan: kecamatan,
    agama: agama,
    pekerjaan: pekerjaan
  })
  fetch('http://localhost:8282/addEmployee', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: str_json,
  })
    .then(response => response.json())
    .then(data => {
      console.log('Succes:', data);
      window.location.href = "/karyawan.html"
    })
    .catch((error) => {
      console.error('Error:', error);
    })
}

function deleteData(id) {

  fetch(`http://localhost:8282/delete/${id}`, {
    method: 'DELETE',
  })
  location.reload(true)
}


function editData(id) {
  console.log(id)
  let name = document.getElementById("name").value
  let agama = document.getElementById("agama").value
  let alamat = document.getElementById("alamat").value
  let jenis_kelamin = document.getElementById("jenis_kelamin").value
  let kecamatan = document.getElementById("kecamatan").value
  let kelurahan = document.getElementById("kelurahan").value
  let pekerjaan = document.getElementById("pekerjaan").value
  let rt = document.getElementById("rt").value
  let rw = document.getElementById("rw").value
  let tanggal_lahir = document.getElementById("tanggal_lahir").value
  let tempat_lahir = document.getElementById("tempat_lahir").value


  let json_update = JSON.stringify({
    name: name,
    tempat_lahir: tempat_lahir,
    tanggal_lahir: tanggal_lahir,
    jenis_kelamin: jenis_kelamin,
    alamat: alamat,
    rt: rt,
    rw: rw,
    kelurahan: kelurahan,
    kecamatan: kecamatan,
    agama: agama,
    pekerjaan: pekerjaan
  })
  fetch(`http://localhost:8282/update/${id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: json_update,
  })
    .then(response => response.json())
    .then(data => {
      console.log('Succes:', data);
      window.location.href = "/karyawan.html"
    })
    .catch((error) => {
      console.error('Error:', error);
    })
  // location.reload(true)

}


// function jumlahEmployee() {
//   console.log(data)
// }
// jumlahEmployee()  